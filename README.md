## ioGame 
拉取 ioGame client 分支，本地编译



### http url

所有 action 类列表
> http://localhost:18080/cmd

action 类下的方法
> http://localhost:18080/cmd/1



## 整体思路

> 使用 websocket 与页面建立连接，使用的是 solon 的 websocket
> 
> 内部维护一个与 ioGame 的游戏对外服的 websocket 连接，并与 solon 建立映射关系
> 
> 内部接收到 ioGame 游戏对外服的消息后，将消息转发到 solon 的 websocket



## 使用与安装

https://www.yuque.com/iohao/game/hgghe9
