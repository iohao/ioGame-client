/*
 * # iohao.com . 渔民小镇
 * Copyright (C) 2021 - 2022 double joker （262610965@qq.com） . All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iohao.game.simulation.client.config;

import com.iohao.game.common.kit.StrKit;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 渔民小镇
 * @date 2022-11-04
 */
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PUBLIC)
public class ClientSimulationConfig {
    String externalIp = "localhost";
    int externalPort = 10100;
    String websocketPath = "websocket";

    int clientSimulationHttpPort = 18080;

    public String getIoGameWsUrl() {
        // 默认不填写的游戏对外服连接地址
        String ioGameWsUrl = "ws://{externalIp}:{externalPort}/{websocketPath}";


        Map<String, Object> map = new HashMap<>(3);
        map.put("externalIp", this.externalIp);
        map.put("externalPort", this.externalPort);
        map.put("websocketPath", this.websocketPath);
        ioGameWsUrl = StrKit.format(ioGameWsUrl, map);

        return ioGameWsUrl;
    }


    private ClientSimulationConfig() {

    }

    public static ClientSimulationConfig me() {
        return Holder.ME;
    }

    /** 通过 JVM 的类加载机制, 保证只加载一次 (singleton) */
    private static class Holder {
        static final ClientSimulationConfig ME = new ClientSimulationConfig();
    }
}