/*
 * # iohao.com . 渔民小镇
 * Copyright (C) 2021 - 2022 double joker （262610965@qq.com） . All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iohao.game.simulation.client.controller;


import com.iohao.game.simulation.client.data.ActionDataManager;
import com.iohao.game.simulation.client.data.dto.ActionControllerDto;
import com.iohao.game.simulation.client.data.dto.ActionDto;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.web.cors.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

/**
 * @author 渔民小镇
 * @date 2022-11-01
 */
@CrossOrigin(origins = "*")
@Controller
public class WebClientController {
    @Mapping("cmd")
    public List<ActionControllerDto> cmd() {
        return ActionDataManager.me().getActionControllerDtoList();
    }

    @Mapping("cmd/{cmd}")
    public List<ActionDto> cmd(int cmd) {
        Map<Integer, List<ActionDto>> actionDtoMap = ActionDataManager.me().getActionDtoMap();
        return actionDtoMap.get(cmd);
    }

//    @Mapping("send/{subCmd}")
//    public void send(@PathVar int subCmd) {
//
//        if (subCmd == 0) {
//            subCmd = 1;
//        }
//
//        HelloReq helloReq = new HelloReq();
//        helloReq.name = "塔姆";
//
//        ClientExternalMessage clientExternalMessage = new ClientExternalMessage();
//        clientExternalMessage.setCmdMerge(CmdKit.merge(1, subCmd));
//        clientExternalMessage.setData(JSON.toJSONString(helloReq));
//
//        String sessionId = ClientSimulation.sessionId;
//        IoGameClientSocket ioGameClientSocket = IoGameClientSocketManager.me().getIoGameClientSocket(sessionId);
//
//        ioGameClientSocket.send(clientExternalMessage);
//    }

}
