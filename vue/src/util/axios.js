import axios from 'axios'

// axios 配置 -> import.meta.env.VITE_URL 环境配置
axios.defaults.baseURL = import.meta.env.VITE_URL
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default axios