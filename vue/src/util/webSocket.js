// 初始化websocket
import {ElMessage} from "element-plus";

export function initWebSocket(ws) {
    let socket = new WebSocket(ws)
    socket.onmessage = function (e) {
        console.log("消息回调")
    }
    socket.onclose = function (e) {
        console.log("关闭回调")
    }
    socket.onopen = function (e) {
        ElMessage.success("连接后台服务器成功！")
        console.log("打开回调")
    }
    // 连接发生错误的回调方法
    socket.onerror = function () {
        console.log("错误回调")
    }
    return socket;
}
