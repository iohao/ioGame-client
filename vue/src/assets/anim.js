// 动画启动样式 - 如果不需要动画  defaultAnimClass = '' 即可
const defaultAnimClass = 'animate__animated'
const animClass = defaultAnimClass+" "
export const bounceInDownList = [defaultAnimClass,"animate__bounceInDown"]
export const bounceInUpList = [defaultAnimClass,"animate__bounceInUp"]

// 动画样式
export const animBounceIn = animClass+'animate__bounceIn'
export const animBounceInUp = animClass+bounceInUpList[1]
export const animBounceInDown = animClass+bounceInDownList[1]


// 根据css标签来渲染动画
export const onCssAnim = (css,list,f)=>{
    let element = document.querySelector(css)
    element.classList.add(list[0], list[1]);
    element.addEventListener('animationend', () => {
        element.classList.remove(list[0],list[1]);
        f()
    });
}