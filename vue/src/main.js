import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
// El
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// Anim
import 'animate.css/animate.css'


// ----- Axios
import VueAxios from 'vue-axios'
import axios from "@/util/axios";
// 自定义
import './assets/index.css'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}


app.use(VueAxios, axios)

app.use(ElementPlus)
app.use(router)

app.mount('#app')


