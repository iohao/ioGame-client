import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import HomeViewPlus from '../views/HomeViewPlus.vue'
import HomeViewLayout from "../views/HomeViewLayout.vue";

const router = createRouter({
  //history: createWebHistory(import.meta.env.BASE_URL),
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeViewLayout
    },
    {
      path: '/plus',
      name: 'plus',
      component: HomeViewPlus
    }
  ]
})

export default router
